// 创建api对象
const CONTACT_API = {
	// 获取联系人方法
	getContactList: {
		method: 'get',
		url: ''
	},
	// 新建联系人 form-data
	newContactForm: {
		method: 'post',
		url: '/contact/new/form'
	},
	// 新建联系人 application/json
	newContactJson: {
		method: 'post',
		url: '/contact/new/json'
	},
	// 编辑联系人
	editContact: {
		method: 'put',
		url: ''
	},
	// 删除联系人
	deleteContact: {
		method: 'delete',
		url: ''
	}
}
// 导出
export default CONTACT_API
