import axios from 'axios'
import service from './contactApi'
import { Toast } from 'vant'
// server 循环遍历 输出不同的请求方法
const instance = axios.create({
	baseURL: 'http://127.0.0.1:8888/api/private/v1/',
	timeout: 1000
})
const Http = {}// 包裹请求方法的容器

// 请求格式/参数的统一
for (const key in service) {
	const api = service[key]// url method
	// async 异步 作用 避免进入回调地狱
	Http[key] = async function (
		params, // 请求参数 get:url ; put、post、patch：（data）；delete：url；
		isFormData = false,
    config = {}// 配置参数
	) {
	 const	url = api.url
	 const newParams = {}

		// content-type 是否是form-data的判断
		if (params && isFormData) {
			newParams = new FormData()
			for (const i in params) {
				newParams.append(i, params[i])
			}
		} else {
			newParams = params
		}
		// 不同请求的判断
		const response = {}// 请求的返回值
		if (api.method === 'put' || api.method === 'post' || api.method === 'patch') {
			try {
				response = await instance[api.method](api.url, newParams, config)
			} catch (erro) {
				response = erro
			}
		} else if (api.method === 'delete' || api.method === 'get') {
			config.params = newParams
			try {
				response = await instance[api.method](api.url, config)
			} catch (err) {
				response = err
			}
		}
		return response// 返回请求的响应值
	}
}

// 拦截器的添加
instance.interceptors.request.use(config => {
	// f发起请求前做些什么
	Toast.loading({
		mask: false,
		duration: 1,
		forbidClick: true, // 禁止点击
		message: '加载中'
})
}, err => {
	// Toast.clear()
	// Toast('请求错误，')
	console.log(err)
})

// 响应拦截器
instance.interceptors.response.use(res => {
	// f发起请求前做些什么
	Toast.clear()
return res.data
}, err => {
	// Toast.clear()
	// Toast('请求错误，')
	console.log(err)
})

export default Http
