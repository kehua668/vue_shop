module.exports = {
	root: true,
	//　添加插件
   "plugins": [
	"vue"
	],
  env: {
    node: true
  },
  extends: [
    'plugin:vue/essential',
    '@vue/standard'
  ],
  parserOptions: {
    parser: 'babel-eslint'
  },
	rules: {
     'indent':0,
      'no-tabs':0,
      'no-mixed-spaces-and-tabs': 0,
     'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
     'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off'
    }
}
